from album import Album
from artist import Artist
from song import Song
from datetime import timedelta, date

#main program

if __name__ == "__main__":
    #create an artist
    frank_ocean = Artist('Frank Ocean', ['Alternative'], ['Frank'])
    s1 = Song('Forrest Gump', timedelta(minutes=3, seconds=27), date(2012, 10, 12))
    s2 = Song('Thinkin Bout You', timedelta(minutes=3, seconds=12), date(2012, 10, 12))
    s3 = Song("Lost",  timedelta(minutes=4, seconds = 55), date(2012, 10, 12))

    sa = Album('Channel Orange', 10/12/2012)
    at = sa.title
    frank_ocean.albums.append(sa)

    for song in frank_ocean.albums:
        if song.title == at:
            sa.song_list += [s1,s2,s3]


    print(frank_ocean.name)
    print(sa.count_songs())
    print(sa.count_duration())
    print(frank_ocean.albums)


    