from album import Album
from artist import Artist
from song import Song
from datetime import timedelta, date
import csv
import pandas as pd

if __name__ == "__main__":
    artists_df = pd.read_csv("data/artists.csv")
    albums_df = pd.read_csv("data/albums.csv")
    songs_df = pd.read_csv("data/songs.csv")

    #print(artists_df)
    # print(albums_df)
    # print(songs_df)

    artist_album_df = pd.read_csv("data/artists_x_albums.csv")
    album_song_df = pd.read_csv("data/albums_x_songs.csv")
    
    #print(artist_album_df)
    #print(album_song_df)

    #song1 = Song(songs_dict['name'][0], songs_dict['duration'][0], songs_dict['release_date'][0])


    #dictionary of dictionaries with 
    albums_dict = albums_df.to_dict(orient='records')

    #dictionary mathing album key to album name
    albums_keys = albums_df.set_index('title').to_dict()['key']


    #dictionary 
    songs_dict = songs_df.to_dict(orient='records')

    #print(albums_dict)
    print(songs_dict)