from datetime import timedelta, date
class Album():
    def __init__(self, title, date, song_list=None):
        self.title = title
        self.date = date
        if song_list is None:
            self.song_list = []
        else:
            self.song_list = song_list

    def __repr__(self):
        return (self.title)

    def count_songs(self):
        return str(len(self.song_list)) + " songs in this album"


    def count_duration(self):
        total = timedelta(minutes=0, seconds=0)
        for song in self.song_list:
            total += song.duration
        return "total duration: " + str(total)