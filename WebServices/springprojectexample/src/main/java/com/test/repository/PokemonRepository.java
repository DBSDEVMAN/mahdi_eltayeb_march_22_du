package com.test.repository;
import com.test.entity.Pokemon;
import org.springframework.data.repository.CrudRepository;
public interface PokemonRepository extends CrudRepository<Pokemon, Integer> {
}
