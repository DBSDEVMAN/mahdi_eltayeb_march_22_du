package com.test.entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="pokemon")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pokemon {
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    public Integer id;
    @Column(name="name")
    String name;
    @Column(name="speed")
    Integer speed;
    @Column(name="attack")
    int attack;
    @Column(name="health")
    int health;
    @Column(name="description")
    String description;
    @Column(name="totalWins")
    int totalWins;
    @Column(name="totalLosses")
    int totalLosses;
    @Column(name="level")
    int level;

}
