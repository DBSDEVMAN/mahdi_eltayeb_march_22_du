package com.test.controller;

import com.test.entity.Customer;
import com.test.services.CustomerService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// *********** GO TO SETTINGS, Plugins and install LOMBOK **************
// Customer class (instructor copy)
// LAB:
//      Go through and create 5 methods
//      using each of the Rest Calls we just learned (Get, post, put, delete)
// GIVEN:
//      You are given the entity and the repository class already setup for you
// GOAL:
//      By the end of this you should be able to get, add, update, and delete customers from the database!


@RestController
@RequestMapping(path = "/customers")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("")
    public ResponseEntity<Iterable<Customer>> getAllCustomers() {
        return new ResponseEntity<Iterable<Customer>>(customerService.getAllCustomers(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<String> createCustomer(@RequestBody Customer customer) {
        var result = customerService.createCustomer(customer);
        if (result.equals("Customer with this ID already exists")) {
            return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable int id) {
        var result = customerService.getCustomer(id);
        if(result == null){
            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Customer>(result, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateCustomer(@PathVariable int id, @RequestBody Customer customer) {
        var result = customerService.updateCustomer(id, customer);
        if(result.equals("Customer Not Updated")){
            return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable int id) {
        var result= customerService.deleteCustomer(id);
        if (result.equals("Customer not found")) {
            return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }



}



