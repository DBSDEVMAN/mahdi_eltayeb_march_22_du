package com.test.controller;
import com.test.entity.Customer;
import com.test.entity.Pokemon;
import com.test.services.PokemonService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(path = "/pokemon")
public class PokemonController {

    @Autowired
    PokemonService pokemonService;


    @GetMapping("")
    public ResponseEntity<Iterable<Pokemon>> getALlPokemon() {
        return new ResponseEntity<Iterable<Pokemon>>(pokemonService.getAllPokemon(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Pokemon> createNewPokemon(@RequestBody Pokemon pokemon) {
        pokemon.setId(null);
        var result = pokemonService.createNewPokemon(pokemon);
//        if (result.equals("Pokemon already exists")) {
//            return new ResponseEntity<Pokemon>(result, HttpStatus.BAD_REQUEST);
//        }
//        else {
//            return new ResponseEntity<String>(result, HttpStatus.OK);
//        }

        return new ResponseEntity<Pokemon>(result, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<Pokemon> getPokemon(@PathVariable int id) {
        var result = pokemonService.getPokemon(id);
        if (result == null) {
            return new ResponseEntity<Pokemon>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Pokemon>(result, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updatePokemon(@PathVariable int id, @RequestBody Pokemon pokemon) {
        var result = pokemonService.updatePokemon(id, pokemon);
        if(result.equals("Pokemon Not Updated")){
            return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePokemon(@PathVariable int id) {
        var result= pokemonService.deletePokemon(id);
        if (result.equals("Pokemon not found")) {
            return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @PutMapping("/battle/{id1}/{id2}")
    public ResponseEntity<String> battlePokemon(@PathVariable int id1, @PathVariable int id2) {
        var result = pokemonService.battlePokemon(id1, id2);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }
}

