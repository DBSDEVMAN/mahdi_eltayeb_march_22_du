package com.test.services;

import com.test.entity.Customer;
import com.test.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// You will need to create the various methods and look into Crud Repositories (similar to JPA)
@Service
@Slf4j // this is used for logging as you see below
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Iterable<Customer> getAllCustomers() {
        log.info("Getting all customers");
        return customerRepository.findAll();
    }

    public String createCustomer(Customer customer) {
        if (customerRepository.existsById(customer.getId())) {
            return "Customer with this ID already exists";
        }
        else {
            customerRepository.save(customer);
            return "Customer created";
        }

    }

    public Customer getCustomer(int id) {
        Optional<Customer> result = customerRepository.findById(id);
            return result.orElse(null);
    }

    public String updateCustomer(int id, Customer customer) {

        if (customerRepository.existsById(customer.getId())) {
            customerRepository.save(customer);
            return "Customer Updated";
        }
        else {
            return "Customer Not Updated";
        }


    }

    public String deleteCustomer(int id) {
        if (customerRepository.existsById((id))) {
            customerRepository.deleteById(id);
            return "Customer deleted.";
        }
        else {
            return "Customer not found";
        }
    }
}
