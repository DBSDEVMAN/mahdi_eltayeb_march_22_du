package com.test.services;

import com.test.entity.Customer;

public interface CustomerService {
    public Iterable<Customer> getAllCustomers();
    String createCustomer(Customer customer);
    public Customer getCustomer(int id);
    public String updateCustomer(int id, Customer customer);
    public String deleteCustomer(int id);
}

