package com.test.services;
import com.test.entity.Pokemon;
import org.springframework.http.ResponseEntity;

public interface PokemonService {
    public Iterable<Pokemon> getAllPokemon();
    public Pokemon createNewPokemon(Pokemon pokemon);
    public Pokemon getPokemon(int id);
    public String updatePokemon(int id, Pokemon pokemon);
    public String deletePokemon(int id);
    public String battlePokemon(int id1, int id2);
}