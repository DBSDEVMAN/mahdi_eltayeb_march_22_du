package com.test.services;

import com.test.entity.Customer;
import com.test.entity.Pokemon;
import com.test.repository.PokemonRepository;
import com.test.services.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;import org.springframework.stereotype.Service;import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;


@Slf4j
@Service
public class PokemonServiceImpl implements PokemonService {
    @Autowired
    PokemonRepository pokemonRepository;

    @Override
    public Iterable<Pokemon> getAllPokemon() {
        log.info("Getting all pokemon");
        return pokemonRepository.findAll();
    }

    public Pokemon createNewPokemon(Pokemon pokemon) {
        pokemon.setId(null);
        Pokemon result = pokemonRepository.save(pokemon);
        return result;
    }

    public Pokemon getPokemon(int id) {
        Optional<Pokemon> result = pokemonRepository.findById(id);
        return result.orElse(null);
    }

    public String updatePokemon(int id, Pokemon pokemon) {

        if (pokemonRepository.existsById(pokemon.getId())) {
            pokemonRepository.save(pokemon);
            return "Pokemon Updated";
        }
        else {
            return "Pokemon Not Updated";
        }


    }

    public String deletePokemon(int id) {
        if (pokemonRepository.existsById((id))) {
            pokemonRepository.deleteById(id);
            return "Pokemon deleted.";
        }
        else {
            return "Pokemon not found";
        }
    }

    public String battlePokemon(int id1, int id2){
        Pokemon pokemon1 = getPokemon(id1);
        Pokemon pokemon2 = getPokemon(id2);

        int tempHealth1 = pokemon1.getHealth();
        int tempHealth2 = pokemon2.getHealth();

        while (tempHealth1 > 0 && tempHealth2> 0) {

            if (pokemon1.getSpeed() > pokemon2.getSpeed()) {
                tempHealth2 -= pokemon1.getAttack();
                log.info(pokemon1.getName() + " has hit " + pokemon2.getName() + " for " + pokemon1.getAttack() + " health points!");
                if (tempHealth2 <= 0) {
                    log.info(pokemon2.getName() + " has fainted.");
                    log.info(pokemon1.getName() + " has won!");

                    pokemon1.setHealth((pokemon1.getHealth())+ 100);
                    pokemon1.setAttack(pokemon1.getAttack() + 50);

                    if (pokemon1.getLevel() < pokemon2.getLevel()) {

                        pokemon1.setLevel(pokemon1.getLevel() + 1);
                        pokemon2.setLevel(pokemon2.getLevel() - 1);
                        pokemon2.setHealth(pokemon2.getHealth() + 50);
                        pokemon2.setAttack(pokemon2.getAttack() + 50);


                    }
                    else if (pokemon1.getLevel() > pokemon2.getLevel()) {

                        pokemon2.setHealth(pokemon2.getHealth() - 50);
                        pokemon2.setAttack(pokemon2.getAttack() - 50);

                    }
                    updatePokemon(pokemon1.getId(), pokemon1);
                    updatePokemon(pokemon2.getId(), pokemon2);

                    if (pokemon2.getLevel() == 0) {
                        deletePokemon(pokemon2.getId());
                        log.info("FATALITY-- " + pokemon2.getName() + "has been yeeted and deleted");
                    }

                    return pokemon1.getName() + "gains 50 attack and 100 health";
                }

                tempHealth1 -= pokemon2.getAttack();
                    log.info(pokemon2.getName() + " has hit " + pokemon1.getName() + " for " + pokemon2.getAttack() + " health points!");
                    if (tempHealth1 <= 0) {
                        log.info(pokemon1.getName() + " has fainted.");
                        log.info(pokemon2.getName() + " has won!");
                        return pokemon2.getName() + "gains 2 attack and 1 health";
                    }
            }

            else {
                tempHealth1 -= pokemon2.getAttack();
                log.info(pokemon2.getName() + " has hit " + pokemon1.getName() + " for " + pokemon2.getAttack() + " health points!");
                if (tempHealth1 <= 0) {
                    log.info(pokemon1.getName() + " has fainted.");
                    log.info(pokemon2.getName() + " has won!");
                    return pokemon2.getName() + "gains 2 attack and 1 health";
                }

                tempHealth2 -= pokemon1.getAttack();
                log.info(pokemon1.getName() + " has hit " + pokemon2.getName() + " for " + pokemon1.getAttack() + " health points!");
                if (tempHealth2 <= 0) {
                    log.info(pokemon2.getName() + " has fainted.");
                    log.info(pokemon1.getName() + " has won!");
                    return pokemon1.getName() + "gains 2 attack and 1 health";
                }
            }

        }


        return "Test";
    }
}